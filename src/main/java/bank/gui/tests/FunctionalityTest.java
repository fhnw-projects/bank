/*
 * Copyright (c) 2019 Fachhochschule Nordwestschweiz (FHNW)
 * All Rights Reserved.
 */

package bank.gui.tests;

import java.util.HashSet;
import java.util.Set;

import bank.Account;
import bank.Bank;
import bank.InactiveException;

public class FunctionalityTest implements BankTest {

	@Override
	public String getName() {
		return "Functionality Test";
	}

	@Override
	public boolean isEnabled(final int size) {
		return size > 0;
	}

	@Override
	public String runTests(final Bank bank, final String currentAccountNumber) throws Exception {
		final Account acc = bank.getAccount(currentAccountNumber);

		String msg = null;

		// Test1: can new accounts be created?
		if (msg == null) {
			final String nr = bank.createAccount("TestUser1");
			if (nr != null && bank.getAccount(nr) != null) {
				bank.closeAccount(nr);
			} else {
				msg = "your implementation does not support\nthe creation of new accounts.";
			}
		}

		// Test2: is active implemented correctly?
		// After closing a deposit on the closed account has to throw an exception
		if (msg == null) {
			final String nr = bank.createAccount("TestUser");
			final Account a = bank.getAccount(nr);
			bank.closeAccount(nr);
			try {
				a.deposit(100);
				msg = "active is not implemented correctly!\n"
					+ "Transactions are not allowed on a closed account.";
			} catch (final InactiveException e) {
				// excpected exception
			}
		}

		// Test3: is it possible to overdraw the account?
		if (msg == null) {
			if (acc.getBalance() < 0) {
				msg = "it is possible to overdraw your account!\n"
						+ "look at the balance of the current account.";
			} else {
				final double amount = acc.getBalance();
				acc.withdraw(amount);
				try {
					final double x = Math.floor(Math.sin(100) * 100) / 10;
					acc.deposit(x);
				} catch (final Exception e) {
					/* ignore the exception */
				}
				if (acc.getBalance() < 0) {
					msg = "it is possible to overdraw your account!\n"
							+ "look at the balance of the current account.";
				} else {
					acc.deposit(amount);
				}
			}
		}

		// Test4: test of transfer
		if (msg == null) {
			final String  n1 = currentAccountNumber;
			final String  n2 = bank.createAccount("Account2");
			final Account a1 = bank.getAccount(n1);
			final Account a2 = bank.getAccount(n2);
			final double a1Balance = a1.getBalance();
			final double a2Balance = a2.getBalance();
			a1.withdraw(a1Balance);
			a2.withdraw(a2Balance);
			a1.deposit(50);
			a2.deposit(50);

			try {
				bank.transfer(a1, a2, -100);
				msg = "oops, your implementation of transfer is not correct!";
			} catch (final Exception e) {
				final double bal1 = a1.getBalance();
				final double bal2 = a2.getBalance();
				if (bal1 != 50 || bal2 != 50) {
					msg = "Although an exception was thrown by transfer, "
							+ "the balances have been changed.\n"
							+ "When an exception is thrown the state must not be changed.";
				}
			}

			if (msg == null) {
				final double bal2 = a2.getBalance();
				a2.withdraw(bal2);
				bank.closeAccount(n2);
				a1.withdraw(50);
				a1.deposit(a1Balance);
			}
		}

		// Test5: can an account with positive balance be closed?
		if (msg == null) {
			final String n = bank.createAccount("Account4");
			final Account a = bank.getAccount(n);
			a.deposit(100);
			final boolean done = bank.closeAccount(n);
			if (done) {
				msg = "Accounts with a positive balance must not be closed!";
			} else {
				a.withdraw(100);
				bank.closeAccount(n);
			}
		}

		// Test6: can an owner open two accounts?
		if (msg == null) {
			final String n1 = bank.createAccount("Meier");
			final String n2 = bank.createAccount("Meier");
			if (n1.equals(n2)) {
				msg = "A user cannot create two accounts using the same name";
			}
			bank.closeAccount(n1);
			bank.closeAccount(n2);
		}

		// Test7: uniqueness of account numbers
		if (msg == null) {
			final String n1 = bank.createAccount("Account1");
			final String n2 = bank.createAccount("Account54039680");

			if (n1.equals(n2)) {
				msg = "different accounts should have different account numbers!";
			}

			bank.closeAccount(n1);
			bank.closeAccount(n2);
		}
		
		// Test8: are arbitrary names supported
		if (msg == null) {
			String name, id;
			Account a;
			name = "Hans Muster";
			id = bank.createAccount(name);
			a = bank.getAccount(id);
			if (!name.equals(a.getOwner())) {
				msg = "not all names are properly supported";
			}
			bank.closeAccount(id);

			name = "Peter Müller;junior";
			id = bank.createAccount(name);
			a = bank.getAccount(id);
			if (!name.equals(a.getOwner())) {
				msg = "not all names are properly supported";
			}
			bank.closeAccount(id);

			name = "Peter:Müller";
			id = bank.createAccount(name);
			a = bank.getAccount(id);
			if (!name.equals(a.getOwner())) {
				msg = "not all names are properly supported";
			}
			bank.closeAccount(id);
		}

//		if (msg == null) {
//			Bank bank = driver.getBank();
//			if (bank != this.bank)
//				msg = "getBank should be implemented as singleton";
//		}

		// Test9: getAccountNumbers should only return the numbers of active accounts
		if (msg == null) {
			final String n = bank.createAccount("Account5");
			final Set<String> s1 = new HashSet<String>(bank.getAccountNumbers());
			bank.closeAccount(n);
			final Set<String> s2 = new HashSet<String>(bank.getAccountNumbers());
			if (s1.equals(s2)) {
				msg = "method getAccountNumbers should only return the numbers of active accounts.";
			}
		}
		
		// Test10: balance of closed accounts should be zero
		if (msg == null) {
			final String n = bank.createAccount("Account6");
			final Account a = bank.getAccount(n);
			bank.closeAccount(n);
			try {
				final double balance = a.getBalance();
				if (balance != 0.0) {
					msg = "balance of a closed account should be zero.";
				}
			} catch (final Exception e) {
				msg = "method getBalance should not throw an Exception.";
			}
		}
		
		// Test11: getAccount must return null if account does not exist
		if(msg == null) {
			final Account a = bank.getAccount("gsdgj234dxsfkjsd"); // assume that this is not a valid number
			if(a != null) {
				msg = "method getAccount must return null if the account does not exist";
			}
		}
		
		// Test12: getAccountNumbes should only contain active accounts
		if(msg == null) {
			final String a1 = bank.createAccount("a1");
			final String a2 = bank.createAccount("a2");
			final String a3 = bank.createAccount("a3");
			final String a4 = bank.createAccount("a4");
			final boolean close2 = bank.closeAccount(a2);
			final boolean close4 = bank.closeAccount(a4);
			if(!close2 || !close4) {
				msg = "accounts could not be closed although their balance is 0";
			} else {
				final Set<String> accountNumbers = bank.getAccountNumbers();
				if(accountNumbers.contains(a2) || accountNumbers.contains(a4)) {
					msg = "method getAccountNumbers should only contain active accounts";
				} else if (!accountNumbers.contains(a1) || !accountNumbers.contains(a3)) {
					msg = "method getAccountNumbers should contain all active accounts";
				}
			}
			if(msg == null) {
				final Account a = bank.getAccount(a2);
				if(a == null) {
					msg = "method getAccount must return all created accounts,\n" +
							"even if they are closed.";
				}
			}
			bank.closeAccount(a1);
			bank.closeAccount(a3);
		}
		
		// Test13: getAccount with invalid argument
		if(msg == null) {
			try {
				final Account a = bank.getAccount("xxx");
				if(a != null) {
					msg = "if bank.getAccount is called with an invalid account number then null must be returned.";
				}
			} catch (final Exception e) {
				msg = "if bank.getAccount is called with an invalid account number then null must be returned,\n" +
						"no exception must be thrown.";
			}
		}
		
		// Test14: closing of closed accounts
		if(msg == null) {
			try {
				final String id = bank.createAccount("a5");
				final boolean close1 = bank.closeAccount(id);
				final boolean close2 = bank.closeAccount(id);
				if(!close1) {
					msg = "An account which has just been created must be closeable. \n" +
							"Your implementation returned false when invoking closeAccount.";
				} else if(close2) {
					msg = "If bank.closeAccount(id) is invoked on an account which is already closed,\n" +
							"then false must be returned (as the close operation was not successful).";
				}
			} catch (final Exception e) {
				msg = "closing a closed account must not end in an exception.";
			}
		}
		
		// Test15: accessing same account twice
		if(msg == null) {
			try {
				final String id = bank.createAccount("a6");
				final Account a1 = bank.getAccount(id);
				final Account a2 = bank.getAccount(id);
				a1.deposit(100);
				if(a2.getBalance() != 100) {
					msg = "if an account is accessed twice with getAccount(id), and if on the first reference\n" +
							"its value is changed, then this change must also be visible over the second reference";
				}
				a1.withdraw(100);
				bank.closeAccount(id);
			} catch (final Exception e) {
				msg = "accessing an account twice with getAccount(id), and then invoking deposit on the first reference\n" +
							"and getBalance on the second reference, this lead to an exception.";
			}
		}
		
		// Test16: Check whether invocation of deposit with a negative argument yields in an IllegalArgumentException
		if (msg == null) {
			final String id16 = bank.createAccount("Test16");
			final Account a16 = bank.getAccount(id16);
			try {
				a16.deposit(100);
				a16.deposit(-50);
				msg = "If method deposit is called with a negative argument, then\n" + "an IllegalArgumemtException must be thrown.";
			} catch (final IllegalArgumentException e) {
				// expected exception
			} catch (final Exception e) {
				msg = "If method deposit is called with a negative argument, then\n" + "an IllegalArgumemtException must be thrown.";
			} finally {
				a16.withdraw(a16.getBalance());
				bank.closeAccount(id16);
			}
		}
		
		// Test17: it should be possible to invoke close for non-existing accounts.
		if (msg == null) {
			try {
				final boolean res = bank.closeAccount("Test17");
				if (res) {
					msg = "If method closeAccount is invoked with an account number\n"
							+ "that does not exist, then this method must return false.";
				}
			} catch (final Exception e) {
				msg = "Invokation of method closeAccount with an account number\n"
						+ "that does not exist should not end in an exception.";
			}
		}
		
		if(msg == null) {
			msg = "Your implementation passed all unit tests";
		}
		
		return msg;
	}
}
