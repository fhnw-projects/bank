package bank.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bank.Account;
import bank.Bank;
import bank.InactiveException;
import bank.local.Driver;

@WebServlet(urlPatterns = { "/*" })
public class BankServlet extends HttpServlet {

    private static final long serialVersionUID = 1224561307999888402L;
    private Bank bank;

    @Override
    public void init() throws ServletException {
        this.bank = new Driver.Bank();
    }

    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, ServletException {

        response.setContentType("text/html");
        final PrintWriter out = response.getWriter();

        final String uri = request.getRequestURI();
        final String dispatchTo = uri.substring(request.getContextPath().length());

        switch (Command.getCommand(dispatchTo)) {
        case GET_ALL_ACCOUNTS:
            this.printAllAccs(out);
            break;
        case GET_ACCOUNT:
            this.printGetAccount(out, request);
            break;
        case CREATE_ACCOUNT:
            this.printCreateHtml(out);
            break;
        case SERVLET_INFO:
            this.printServletInfo(out, request);
            break;
        case INVALID_ROUTING:
        default:
            this.printErrorHtml(out, "Invalid routing");
        }

    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        final PrintWriter out = response.getWriter();

        final String uri = request.getRequestURI();
        final String dispatchTo = uri.substring(request.getContextPath().length());

        switch (Command.getCommand(dispatchTo)) {
        case CREATE_ACCOUNT:
            this.createAccount(request, response);
            break;
        case INVALID_ROUTING:
        default:
            this.printErrorHtml(out, "Invalid routing");
        }

    }

    private void printGetAccount(final PrintWriter out, final HttpServletRequest request) {
        final String id = request.getParameter("id");
        if (id == null) {
            this.printErrorHtml(out, "Bad Request");
        } else {
            try {
                final Account acc = this.bank.getAccount(id);
                out.println("<html><body>");
                out.println("<h2>" + acc.getOwner() + "</h2>");
                out.println("<h2>" + acc.getNumber() + "</h2>");
                out.println("<h2>" + acc.getBalance() + "</h2>");
                out.println("</body></html>");

            } catch (final IOException e) {
                this.printErrorHtml(out, e.getMessage());
            }
        }

    }

    private void createAccount(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {

        final String owner = request.getParameter("owner");
        final String balance = request.getParameter("balance");

        final Optional<Double> balanceOpt = this.tryGetDouble(balance);

        if (owner != null && balance != null && balanceOpt.isPresent()) {
            try {
                final String number = this.bank.createAccount(owner);
                this.bank.getAccount(number).deposit(balanceOpt.get());
                response.sendRedirect(request.getContextPath() + "/account/all");
            } catch (IOException | IllegalArgumentException | InactiveException e) {
                this.printErrorHtml(response.getWriter(), e.getMessage());
            }
        } else {
            this.printErrorHtml(response.getWriter(), "Invalid params");
        }

    }

    private Optional<Double> tryGetDouble(final String balance) {
        try {
            return Optional.of(Double.parseDouble(balance));
        } catch (NumberFormatException | NullPointerException ex) {
            return Optional.empty();
        }
    }

    private void printAllAccs(final PrintWriter out) {
        try {
            final Set<String> accs = this.bank.getAccountNumbers();
            out.println("<html><body>");
            out.println("<h1>Accounts</h1>");
            for (final String accId : accs) {
                final Account acc = this.bank.getAccount(accId);
                out.println("<a href=\"get?id=" + accId + "\">" + acc.getOwner() + "</a><br/>");
            }
            out.println("<a href=\"create\">Create Account</a>");
            out.println("</body></html>");
        } catch (final IOException e) {
            this.printErrorHtml(out, e.getMessage());
        }

    }

    private void printServletInfo(final PrintWriter out, final HttpServletRequest request) {
        out.println("<html><body><pre>");
        out.println("Properties:");
        out.println("getMethod:        " + request.getMethod());
        out.println("getContentLength: " + request.getContentLength());
        out.println("getContentType:   " + request.getContentType());
        out.println("getProtocol:      " + request.getProtocol());
        out.println("getRemoteAddr:    " + request.getRemoteAddr());
        out.println("getRemotePort:    " + request.getRemotePort());
        out.println("getRemoteHost:    " + request.getRemoteHost());
        out.println("getRemoteUser:    " + request.getRemoteUser());
        out.println("getServerName:    " + request.getServerName());
        out.println("getAuthType:      " + request.getAuthType());
        out.println("getQueryString:   " + request.getQueryString());
        out.println("getRequestURI:    " + request.getRequestURI());
        out.println("getRequestURL:    " + request.getRequestURL());
        out.println("getServletPath:   " + request.getServletPath());
        out.println("getContextPath:   " + request.getContextPath());
        out.println("</pre></body></html>");
    }

    private void printErrorHtml(final PrintWriter out, final String message) {
        out.println("<html><body>");
        out.println("<h1>Error: </h1>");
        out.println("<h2>" + message + "</h2>");
        out.println("</body></html>");
    }

    private void printCreateHtml(final PrintWriter out) {

        out.println("<html><body>");
        out.println("<h1>Create Account</h1>");
        out.println("<form action=\"create\" method=\"POST\">");
        out.println("<label>Owner</label>");
        out.println("<input type=\"text\" name=\"owner\" />");
        out.println("<label>Balance</label>");
        out.println("<input type=\"text\" name=\"balance\" />");
        out.println("<input type=\"submit\" />");
        out.println("</form>");
        out.println("<a href=\"all\">Show all accounts</a>");
        out.println("</body></html>");
    }

    public static enum Command {
        GET_ALL_ACCOUNTS("/account/all"), GET_ACCOUNT("/account/get"), CREATE_ACCOUNT("/account/create"), SERVLET_INFO(
                "/info"), INVALID_ROUTING("/error");

        private final String servletPath;

        Command(final String servletPath) {
            this.servletPath = servletPath;
        }

        public static Command getCommand(final String uri) {
            for (final Command c : values()) {
                if (c.servletPath.equals(uri)) {
                    return c;
                }
            }
            return INVALID_ROUTING;
        }

    }
}
