package bank.rest.server;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class AppExceptionHandler implements ExceptionMapper<AppException> {

	@Override
	public Response toResponse(final AppException ex) {
		return Response.status(ex.getStatus()).entity(new ApiResponse(ex)).type(MediaType.APPLICATION_JSON).build();
	}

}
