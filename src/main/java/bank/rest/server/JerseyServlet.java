package bank.rest.server;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

import org.glassfish.jersey.servlet.ServletContainer;

@WebServlet(urlPatterns = "/rest/*", initParams = {
		@WebInitParam(name = "javax.ws.rs.Application", value = "bank.rest.server.BankApplication") }, loadOnStartup = 1)
public class JerseyServlet extends ServletContainer {

	private static final long serialVersionUID = -959545133094062056L;

}
