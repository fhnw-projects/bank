package bank.rest.server;

import javax.ws.rs.core.Response.Status;

public class ApiResponse {

    private String message;
    private Status status;
    private int statusCode;
    private String exceptionType;

    public ApiResponse(final AppException ex) {
        this.message = ex.getMessage();
        this.status = ex.getStatus();
        this.statusCode = ex.getStatus().getStatusCode();
        if (ex.getCause() != null) {
            this.exceptionType = ex.getCause().toString();
        }
    }

    public ApiResponse(final String message, final Status status) {
        this.message = message;
        this.status = status;
        this.statusCode = status.getStatusCode();
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(final int statusCode) {
        this.statusCode = statusCode;
    }

    public String getExceptionType() {
        return this.exceptionType;
    }

}
