package bank.rest.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import bank.Account;
import bank.Bank;
import bank.InactiveException;
import bank.OverdrawException;

@Singleton
@Path("/account")
public class AccountResource {

    private final Bank bank;

    public AccountResource(final Bank bank) {
        this.bank = bank;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Account> getAccounts() throws AppException {

        final List<Account> accs = new ArrayList<>();

        try {
            final Set<String> accNumbers = this.bank.getAccountNumbers();
            for (final String accNumber : accNumbers) {
                accs.add(this.bank.getAccount(accNumber));
            }
        } catch (final IOException e) {
            throw new AppException("fail during getting all accounts", e, Status.INTERNAL_SERVER_ERROR);
        }

        return accs;
    }

    @GET
    @Path("/{number}")
    @Produces(MediaType.APPLICATION_JSON)
    public Account getAccountById(@PathParam("number") final String number) throws AppException {

        try {
            final Optional<Account> acc = this.tryGetAccountByNumber(number);
            if (acc.isPresent()) {
                return acc.get();
            }
            throw new AppException("Acc with id: " + number + " not found", null, Status.BAD_REQUEST);
        } catch (final IOException e) {
            throw new AppException("Fail during getting account by id", e, Status.INTERNAL_SERVER_ERROR);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String createAccount(final String owner) throws AppException {

        if (owner == null) {
            throw new AppException("missing param: 'owner'", null, Status.BAD_REQUEST);
        }
        try {
            return this.bank.createAccount(owner);
        } catch (final IOException e) {
            throw new AppException("Fail during creating account", e, Status.INTERNAL_SERVER_ERROR);
        }
    }

    @DELETE
    @Path("/{number}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ApiResponse closeAccount(@PathParam("number") final String number) throws AppException {

        try {
            final boolean success = this.bank.closeAccount(number);
            if (success) {
                return new ApiResponse("Account successfully closed", Status.OK);
            }
            throw new AppException("Unable to close account", null, Status.PRECONDITION_FAILED);
        } catch (final IOException e) {
            throw new AppException(e.getMessage(), e, Status.INTERNAL_SERVER_ERROR);
        }
    }

    @PUT
    @Path("/transfer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ApiResponse transfer(final Transfer tran) throws AppException {

        if (tran.getNumberA() == null || tran.getNumberB() == null || tran.getAmount() == 0) {
            throw new AppException("invalid params", null, Status.BAD_REQUEST);
        }

        try {
            final Account accA = this.bank.getAccount(tran.getNumberA());
            final Account accB = this.bank.getAccount(tran.getNumberB());
            this.bank.transfer(accA, accB, tran.getAmount());
        } catch (final IOException e) {
            throw new AppException(e.getMessage(), e, Status.INTERNAL_SERVER_ERROR);
        } catch (final IllegalArgumentException | InactiveException | OverdrawException e) {
            throw new AppException(e.getMessage(), e, Status.BAD_REQUEST);
        }

        return new ApiResponse("amount successfully transferred", Status.OK);
    }

    @PUT
    @Path("/{number}/deposit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ApiResponse deposit(@PathParam("number") final String number, final double amount) throws AppException {

        try {
            final Optional<Account> acc = this.tryGetAccountByNumber(number);
            if (acc.isPresent()) {
                try {
                    acc.get().deposit(amount);
                    return new ApiResponse("successfully deposited amount", Status.OK);
                } catch (IllegalArgumentException | InactiveException e) {
                    throw new AppException(e.getMessage(), e, Status.BAD_REQUEST);
                }
            }
            throw new AppException("Acc with id: " + number + " not found", null, Status.BAD_REQUEST);
        } catch (final IOException e) {
            throw new AppException("Fail during getting account by id", e, Status.INTERNAL_SERVER_ERROR);
        }
    }

    @PUT
    @Path("/{number}/withdraw")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ApiResponse withdraw(@PathParam("number") final String number, final double amount) throws AppException {

        try {
            final Optional<Account> acc = this.tryGetAccountByNumber(number);
            if (acc.isPresent()) {
                try {
                    acc.get().withdraw(amount);
                    return new ApiResponse("successfully deposited amount", Status.OK);
                } catch (IllegalArgumentException | InactiveException | OverdrawException e) {
                    throw new AppException(e.getMessage(), e, Status.BAD_REQUEST);
                }
            }
            throw new AppException("Acc with id: " + number + " not found", null, Status.BAD_REQUEST);
        } catch (final IOException e) {
            throw new AppException("Fail during getting account by id", e, Status.INTERNAL_SERVER_ERROR);
        }
    }

    private Optional<Account> tryGetAccountByNumber(final String id) throws IOException {
        return Optional.ofNullable(this.bank.getAccount(id));
    }

    public static class Transfer {

        private String numberA;
        private String numberB;
        private double amount;

        public String getNumberA() {
            return this.numberA;
        }

        public void setNumberA(final String numberA) {
            this.numberA = numberA;
        }

        public String getNumberB() {
            return this.numberB;
        }

        public void setNumberB(final String numberB) {
            this.numberB = numberB;
        }

        public double getAmount() {
            return this.amount;
        }

        public void setAmount(final double amount) {
            this.amount = amount;
        }

    }

}
