package bank.rest.server;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import bank.Bank;
import bank.local.Driver;

public class BankApplication extends Application {
	private final Set<Object> singletons = new HashSet<Object>();
	private final Set<Class<?>> classes = new HashSet<Class<?>>();

	public BankApplication() {
		final Bank bank = new Driver.Bank();
		this.singletons.add(new AccountResource(bank));
		this.singletons.add(new AppExceptionHandler());
	}

	@Override
	public Set<Class<?>> getClasses() {
		return this.classes;
	}

	@Override
	public Set<Object> getSingletons() {
		return this.singletons;
	}
}
