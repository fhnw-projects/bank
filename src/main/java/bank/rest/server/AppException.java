package bank.rest.server;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

@Provider
public class AppException extends Exception {

	private static final long serialVersionUID = 2789713194264000742L;

	private Status status;
	
	public AppException(final String errorMessage, final Throwable t, final Status status) {
		super(errorMessage, t);
		this.setStatus(status);
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(final Status status) {
		this.status = status;
	}

}
