package bank.rest.client;

public class AccountResponse {

    private String number;
    private String owner;
    private double balance;
    private boolean active;

    public String getNumber() {
        return this.number;
    }

    public void setNumber(final String number) {
        this.number = number;
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(final String owner) {
        this.owner = owner;
    }

    public double getBalance() {
        return this.balance;
    }

    public void setBalance(final double balance) {
        this.balance = balance;
    }

    public boolean isActive() {
        return this.active;
    }

}
