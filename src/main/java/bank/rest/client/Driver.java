package bank.rest.client;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import bank.BankDriver;
import bank.InactiveException;
import bank.OverdrawException;

public class Driver implements BankDriver {

    private Bank bank;
    private Client client;

    @Override
    public void connect(final String[] args) throws IOException {

        this.client = ClientBuilder.newClient();
        final WebTarget webTarget = this.client.target(args[0]);
        this.bank = new Bank(webTarget);
    }

    @Override
    public void disconnect() throws IOException {
        this.client.close();
    }

    @Override
    public Bank getBank() {
        return this.bank;
    }

    public static class Bank implements bank.Bank {

        private final WebTarget webTarget;
        private static String ACCOUNT_PATH = "account";
        private static String TRANSFER_PATH = "transfer";

        public Bank(final WebTarget webTarget) {
            this.webTarget = webTarget;
        }

        public static AccountResponse getAccount(final WebTarget webTarget, final String number) throws IOException {

            final Builder builder = webTarget.path(ACCOUNT_PATH).path(number).request(MediaType.APPLICATION_JSON);
            final Response response = builder.get();
            if (response.getStatus() != 200) {
                final ApiResponse ex = response.readEntity(ApiResponse.class);
                if (ex.getStatusCode() == Status.BAD_REQUEST.getStatusCode()) {
                    return null;
                }
                throw new IOException(ex.getMessage());
            }

            return response.readEntity(AccountResponse.class);
        }

        @Override
        public String createAccount(final String owner) throws IOException {

            final Builder builder = this.webTarget.path(ACCOUNT_PATH).request(MediaType.APPLICATION_JSON);
            final Response response = builder.post(Entity.entity(owner, MediaType.APPLICATION_JSON));
            if (response.getStatus() != 200) {
                final ApiResponse ex = response.readEntity(ApiResponse.class);
                if (ex.getStatusCode() == Status.BAD_REQUEST.getStatusCode()) {
                    return null;
                }
                throw new IOException(ex.getMessage());
            }

            return response.readEntity(String.class);
        }

        @Override
        public boolean closeAccount(final String number) throws IOException {

            final Builder builder = this.webTarget.path(ACCOUNT_PATH).path(number).request(MediaType.APPLICATION_JSON);
            final Response response = builder.delete();
            if (response.getStatus() != 200) {
                final ApiResponse ex = response.readEntity(ApiResponse.class);
                if (ex.getStatusCode() == Status.PRECONDITION_FAILED.getStatusCode()) {
                    return false;
                }
                throw new IOException(ex.getMessage());
            }

            return true;
        }

        @Override
        public Set<String> getAccountNumbers() throws IOException {

            final Builder builder = this.webTarget.path(ACCOUNT_PATH).request(MediaType.APPLICATION_JSON);
            final Response response = builder.get();
            if (response.getStatus() != 200) {
                final ApiResponse ex = response.readEntity(ApiResponse.class);
                throw new IOException(ex.getMessage());
            }
            final List<AccountResponse> accs = response.readEntity(new GenericType<List<AccountResponse>>() {
            });

            return accs.stream().map(e -> e.getNumber()).collect(Collectors.toSet());
        }

        @Override
        public Account getAccount(final String number) throws IOException {
            final AccountResponse acc = Bank.getAccount(this.webTarget, number);
            if(acc == null) {
                return null;
            }
            return new Account(this.webTarget, acc.getNumber());
        }

        @Override
        public void transfer(final bank.Account a, final bank.Account b, final double amount)
                throws IOException, IllegalArgumentException, OverdrawException, InactiveException {

            final Builder builder = this.webTarget.path(ACCOUNT_PATH).path(TRANSFER_PATH)
                    .request(MediaType.APPLICATION_JSON);
            final TransferRequest trans = new TransferRequest(a.getNumber(), b.getNumber(), amount);
            final Response response = builder.put(Entity.entity(trans, MediaType.APPLICATION_JSON));
            if (response.getStatus() != 200) {
                final ApiResponse ex = response.readEntity(ApiResponse.class);
                if (ex.getStatusCode() == Status.BAD_REQUEST.getStatusCode()) {
                    if (IllegalArgumentException.class.getName().equals(ex.getExceptionType())) {
                        throw new IllegalArgumentException(ex.getMessage());
                    } else if (OverdrawException.class.getName().equals(ex.getExceptionType())) {
                        throw new OverdrawException(ex.getMessage());
                    } else if (InactiveException.class.getName().equals(ex.getExceptionType())) {
                        throw new InactiveException(ex.getMessage());
                    }
                }
                throw new IOException(ex.getMessage());
            }
        }

        public WebTarget getWebTarget() {
            return this.webTarget;
        }

    }

    public static class Account implements bank.Account {

        private final String number;
        private final WebTarget webTarget;

        private static String DEPOSIT_PATH = "deposit";
        private static String WITHDRAW_PATH = "withdraw";

        public Account(final WebTarget webTarget, final String number) {
            this.webTarget = webTarget;
            this.number = number;
        }

        @Override
        public String getNumber() {
            return this.number;
        }

        @Override
        public String getOwner() throws IOException {
            final AccountResponse acc = Bank.getAccount(this.webTarget, this.number);
            return acc.getOwner();
        }

        @Override
        public boolean isActive() throws IOException {
            final AccountResponse acc = Bank.getAccount(this.webTarget, this.number);
            return acc.isActive();
        }

        @Override
        public double getBalance() throws IOException {
            final AccountResponse acc = Bank.getAccount(this.webTarget, this.number);
            return acc.getBalance();
        }

        @Override
        public void deposit(final double amount) throws IOException, IllegalArgumentException, InactiveException {

            final Builder builder = this.webTarget.path(Driver.Bank.ACCOUNT_PATH).path(this.number).path(DEPOSIT_PATH)
                    .request(MediaType.APPLICATION_JSON);
            final Response response = builder.put(Entity.entity(amount, MediaType.APPLICATION_JSON));
            if (response.getStatus() != 200) {
                final ApiResponse ex = response.readEntity(ApiResponse.class);
                if (ex.getStatusCode() == Status.BAD_REQUEST.getStatusCode()) {
                    if (IllegalArgumentException.class.getName().equals(ex.getExceptionType())) {
                        throw new IllegalArgumentException(ex.getMessage());
                    } else if (InactiveException.class.getName().equals(ex.getExceptionType())) {
                        throw new InactiveException(ex.getMessage());
                    }
                }
                throw new IOException(ex.getMessage());
            }
        }

        @Override
        public void withdraw(final double amount)
                throws IOException, IllegalArgumentException, OverdrawException, InactiveException {

            final Builder builder = this.webTarget.path(Driver.Bank.ACCOUNT_PATH).path(this.number).path(WITHDRAW_PATH)
                    .request(MediaType.APPLICATION_JSON);
            final Response response = builder.put(Entity.entity(amount, MediaType.APPLICATION_JSON));
            if (response.getStatus() != 200) {
                final ApiResponse ex = response.readEntity(ApiResponse.class);
                if (ex.getStatusCode() == Status.BAD_REQUEST.getStatusCode()) {
                    if (IllegalArgumentException.class.getName().equals(ex.getExceptionType())) {
                        throw new IllegalArgumentException(ex.getMessage());
                    } else if (InactiveException.class.getName().equals(ex.getExceptionType())) {
                        throw new InactiveException(ex.getMessage());
                    } else if (OverdrawException.class.getName().equals(ex.getExceptionType())) {
                        throw new OverdrawException(ex.getMessage());
                    }

                }
                throw new IOException(ex.getMessage());
            }
        }

        public WebTarget getWebTarget() {
            return this.webTarget;
        }

    }

}
