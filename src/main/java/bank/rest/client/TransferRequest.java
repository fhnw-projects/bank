package bank.rest.client;

public class TransferRequest {

    private String numberA;
    private String numberB;
    private double amount;

    public TransferRequest(final String numberA, final String numberB, final double amount) {
        this.numberA = numberA;
        this.numberB = numberB;
        this.amount = amount;
    }

    public String getNumberA() {
        return this.numberA;
    }

    public void setNumberA(final String numberA) {
        this.numberA = numberA;
    }

    public String getNumberB() {
        return this.numberB;
    }

    public void setNumberB(final String numberB) {
        this.numberB = numberB;
    }

    public double getAmount() {
        return this.amount;
    }

    public void setAmount(final double amount) {
        this.amount = amount;
    }

}
