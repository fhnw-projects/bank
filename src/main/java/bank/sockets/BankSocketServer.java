package bank.sockets;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Set;

import bank.Account;
import bank.Bank;
import bank.InactiveException;
import bank.OverdrawException;
import bank.sockets.Driver.SocketCommand;

public class BankSocketServer {
    public static void main(final String args[]) throws IOException {
        final int port = 1234;
        final Bank bank = new bank.local.Driver.Bank();
        try (ServerSocket server = new ServerSocket(port)) {
            System.out.println("Startet Bank Socket Server on port " + port);
            while (true) {
                final Socket socket = server.accept();
                final Thread t = new Thread(new BankHandler(socket, bank));
                t.start();
            }
        }
    }

    private static class BankHandler implements Runnable {

        private final Bank bank;
        private final ObjectInputStream in;
        private final ObjectOutputStream out;

        public BankHandler(final Socket socket, final Bank bank) throws IOException {
            this.bank = bank;
            this.out = new ObjectOutputStream(socket.getOutputStream());
            this.in = new ObjectInputStream(socket.getInputStream());
        }

        @Override
		public void run() {
            while (true) {
                try {
                    final int command = this.in.readInt();
                    switch (SocketCommand.getSocketCommand(command)) {
                    case CREATE_ACCOUNT: {
                        final String owner = this.in.readUTF();
                        final String accNumber = this.bank.createAccount(owner);
                        this.out.writeUTF(accNumber == null ? "" : accNumber);
                        this.out.flush();
                        break;
                    }
                    case CLOSE_ACCOUNT: {
                        final String accNumber = this.in.readUTF();
                        final boolean isClosed = this.bank.closeAccount(accNumber);
                        this.out.writeBoolean(isClosed);
                        this.out.flush();
                        break;
                    }
                    case GET_ACCOUNT_NUMBERS: {
                        final Set<String> accs = this.bank.getAccountNumbers();
                        this.out.writeInt(accs.size());
                        for (final String acc : accs) {
                            this.out.writeUTF(acc);
                        }
                        this.out.flush();
                        break;
                    }
                    case GET_ACCOUNT: {
                        final String accNumber = this.in.readUTF();
                        final Account acc = this.bank.getAccount(accNumber);
                        this.out.writeObject(acc);
                        this.out.flush();
                        break;
                    }
                    case TRANSFER: {
                        try {
                            final Account proxyA = (Account) this.in.readObject();
                            final Account proxyB = (Account) this.in.readObject();
                            final double amount = this.in.readDouble();
                            final Account a = this.bank.getAccount(proxyA.getNumber());
                            final Account b = this.bank.getAccount(proxyB.getNumber());
                            this.bank.transfer(a, b, amount);
                            this.out.writeInt(0);
                        } catch (final ClassNotFoundException e) {
                            throw new IOException();
                        } catch (final IllegalArgumentException e) {
                            this.out.writeInt(1);
                        } catch (final OverdrawException e) {
                            this.out.writeInt(2);
                        } catch (final InactiveException e) {
                            this.out.writeInt(3);
                        }
                        this.out.flush();
                        break;
                    }
                    case GET_OWNER: {
                        final String number = this.in.readUTF();
                        final String owner = this.bank.getAccount(number).getOwner();
                        this.out.writeUTF(owner);
                        this.out.flush();
                        break;
                    }
                    case IS_ACTIVE: {
                        final String number = this.in.readUTF();
                        final boolean isActive = this.bank.getAccount(number).isActive();
                        this.out.writeBoolean(isActive);
                        this.out.flush();
                        break;
                    }
                    case DEPOSIT: {
                        final String number = this.in.readUTF();
                        final double amount = this.in.readDouble();
                        try {
                            this.bank.getAccount(number).deposit(amount);
                            this.out.writeInt(0);
                        } catch (final IllegalArgumentException e) {
                            this.out.writeInt(1);
                        } catch (final InactiveException e) {
                            this.out.writeInt(2);
                        }
                        this.out.flush();
                        break;
                    }
                    case WITHDRAW: {
                        final String number = this.in.readUTF();
                        final double amount = this.in.readDouble();
                        try {
                            this.bank.getAccount(number).withdraw(amount);
                            this.out.writeInt(0);
                        } catch (final IllegalArgumentException e) {
                            this.out.writeInt(1);
                        } catch (final InactiveException e) {
                            this.out.writeInt(2);
                        } catch (final OverdrawException e) {
                            this.out.writeInt(3);
                        }
                        this.out.flush();
                        break;
                    }
                    case GET_BALANCE: {
                        final String number = this.in.readUTF();
                        final double balance = this.bank.getAccount(number).getBalance();
                        this.out.writeDouble(balance);
                        this.out.flush();
                        break;
                    }
                    case INVALID:
                    default:
                        throw new RuntimeException();
                    }

                    System.out.println("Command: " + command + " executed.");
                } catch (final IOException e) {
                    System.out.println("Exception occured during proccessing command.");
                }
            }
        }
    }
}
