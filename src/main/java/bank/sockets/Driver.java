package bank.sockets;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

import bank.Account;
import bank.Bank;
import bank.BankDriver;
import bank.InactiveException;
import bank.OverdrawException;

public class Driver implements BankDriver {

    private Bank bank;
    private Socket socket;

    @Override
    public void connect(final String[] args) throws IOException {

        String host = "localhost";
        int port = 1234;
        if (args.length > 0) {
            host = args[0];
        }
        if (args.length > 1) {
            port = Integer.parseInt(args[1]);
        }

        System.out.println("connecting to " + host + ":" + port);
        this.socket = new Socket(host, port);
        final ObjectOutputStream out = new ObjectOutputStream(this.socket.getOutputStream());
        final ObjectInputStream in = new ObjectInputStream(this.socket.getInputStream());
        System.out.println("creating bank");
        this.bank = new SocketBank(in, out);
        System.out.println("connected to " + this.socket.getRemoteSocketAddress());
    }

    @Override
    public void disconnect() throws IOException {
        this.socket.close();
        this.bank = null;
        System.out.println("disconnected");
    }

    @Override
    public Bank getBank() {
        return this.bank;
    }

    public static class SocketBank implements Bank {

        private final ObjectInputStream in;
        private final ObjectOutputStream out;

        public SocketBank(final ObjectInputStream in, final ObjectOutputStream out) {
            this.in = in;
            this.out = out;
        }

        @Override
        public String createAccount(final String owner) throws IOException {

            this.out.writeInt(SocketCommand.CREATE_ACCOUNT.getCommandId());
            this.out.writeUTF(owner);
            this.out.flush();
            final String id = this.in.readUTF();

            return id.equals("") ? null : id;
        }

        @Override
        public boolean closeAccount(final String number) throws IOException {

            this.out.writeInt(SocketCommand.CLOSE_ACCOUNT.getCommandId());
            this.out.writeUTF(number);
            this.out.flush();

            return this.in.readBoolean();
        }

        @Override
        public Set<String> getAccountNumbers() throws IOException {

            final Set<String> accNumbers = new HashSet<String>();
            this.out.writeInt(SocketCommand.GET_ACCOUNT_NUMBERS.commandId);
            this.out.flush();
            final int size = this.in.readInt();
            System.out.println(size);
            for (int i = 0; i < size; i++) {
                accNumbers.add(this.in.readUTF());
            }
            return accNumbers;
        }

        @Override
        public SocketAccount getAccount(final String number) throws IOException {

            this.out.writeInt(SocketCommand.GET_ACCOUNT.commandId);
            this.out.writeUTF(number);
            this.out.flush();

            try {
                final Account acc = (Account) this.in.readObject();
                if(acc == null) {
                    return null;
                }
                final SocketAccount proxyAcc = new SocketAccount(this.in, this.out);
                proxyAcc.number = acc.getNumber();
                return proxyAcc;
            } catch (final ClassNotFoundException e) {
                return null;
            }
        }

        @Override
        public void transfer(final Account a, final Account b, final double amount)
                throws IOException, IllegalArgumentException, OverdrawException, InactiveException {

            this.out.writeInt(SocketCommand.TRANSFER.commandId);
            this.out.writeObject(a);
            this.out.writeObject(b);
            this.out.writeDouble(amount);
            this.out.flush();
            final int result = this.in.readInt();
            switch (result) {
            case 0:
                break;
            case 1:
                throw new IllegalArgumentException();
            case 2:
                throw new OverdrawException();
            case 3:
                throw new InactiveException();
            }
        }
    }

    public static class SocketAccount implements Account, Serializable {

        private static final long serialVersionUID = -6077074255476359152L;

        private transient ObjectInputStream in;
        private transient ObjectOutputStream out;

        private String number;

        public SocketAccount(final ObjectInputStream in, final ObjectOutputStream out) {
            this.in = in;
            this.out = out;
        }

        @Override
        public String getNumber() throws IOException {
            return this.number;
        }

        @Override
        public String getOwner() throws IOException {

            this.out.writeInt(SocketCommand.GET_OWNER.commandId);
            this.out.writeUTF(this.number);
            this.out.flush();

            return this.in.readUTF();
        }

        @Override
        public boolean isActive() throws IOException {

            this.out.writeInt(SocketCommand.IS_ACTIVE.commandId);
            this.out.writeUTF(this.number);
            this.out.flush();

            return this.in.readBoolean();
        }

        @Override
        public void deposit(final double amount) throws IOException, IllegalArgumentException, InactiveException {

            this.out.writeInt(SocketCommand.DEPOSIT.commandId);
            this.out.writeUTF(this.number);
            this.out.writeDouble(amount);
            this.out.flush();
            final int result = this.in.readInt();
            switch (result) {
            case 0:
                break;
            case 1:
                throw new IllegalArgumentException();
            case 2:
                throw new InactiveException();
            }
        }

        @Override
        public void withdraw(final double amount)
                throws IOException, IllegalArgumentException, OverdrawException, InactiveException {

            this.out.writeInt(SocketCommand.WITHDRAW.commandId);
            this.out.writeUTF(this.number);
            this.out.writeDouble(amount);
            this.out.flush();
            final int result = this.in.readInt();
            switch (result) {
            case 0:
                break;
            case 1:
                throw new IllegalArgumentException();
            case 2:
                throw new InactiveException();
            case 3:
                throw new OverdrawException();
            }
        }

        @Override
        public double getBalance() throws IOException {

            this.out.writeInt(SocketCommand.GET_BALANCE.commandId);
            this.out.writeUTF(this.number);
            this.out.flush();

            return this.in.readDouble();
        }

    }

    public enum SocketCommand {
        CREATE_ACCOUNT(1), CLOSE_ACCOUNT(2), GET_ACCOUNT_NUMBERS(3), GET_ACCOUNT(4), TRANSFER(5), GET_NUMBER(
                6), GET_OWNER(7), IS_ACTIVE(8), DEPOSIT(9), WITHDRAW(10), GET_BALANCE(11), INVALID(99);

        private final int commandId;

        SocketCommand(final int commandId) {
            this.commandId = commandId;
        }

        public int getCommandId() {
            return this.commandId;
        }

        public static SocketCommand getSocketCommand(final int index) {
            for (final SocketCommand t : values()) {
                if (t.commandId == index) {
                    return t;
                }
            }
            return INVALID;
        }

    }
}
