/*
 * Copyright (c) 2019 Fachhochschule Nordwestschweiz (FHNW)
 * All Rights Reserved. 
 */

package bank.local;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import bank.InactiveException;
import bank.OverdrawException;

public class Driver implements bank.BankDriver {
    private Bank bank = null;

    @Override
    public void connect(final String[] args) {
        this.bank = new Bank();
        System.out.println("connected...");
    }

    @Override
    public void disconnect() {
        this.bank = null;
        System.out.println("disconnected...");
    }

    @Override
    public Bank getBank() {
        return this.bank;
    }

    public static class Bank implements bank.Bank {

        private final Map<String, Account> accounts = new HashMap<>();

        @Override
        public Set<String> getAccountNumbers() {
            return this.accounts.entrySet().stream().filter(e -> e.getValue().isActive()).map(e -> e.getKey())
                    .collect(Collectors.toSet());
        }

        @Override
        public String createAccount(final String owner) {

            final Account acc = new Account(owner);
            this.accounts.put(acc.getNumber(), acc);

            return acc.getNumber();
        }

        @Override
        public boolean closeAccount(final String number) {

            final Account acc = this.accounts.get(number);

            if (acc != null && acc.isActive() && acc.getBalance() == 0) {
                acc.active = false;
                return true;
            }

            return false;
        }

        @Override
        public bank.Account getAccount(final String number) {
            return this.accounts.get(number);
        }

        @Override
        public void transfer(final bank.Account from, final bank.Account to, final double amount)
                throws IOException, IllegalArgumentException, InactiveException, OverdrawException {

            if (!from.isActive() || !to.isActive()) {
                throw new InactiveException();
            }

            from.withdraw(amount);
            to.deposit(amount);
        }

    }

    static class Account implements bank.Account {

        private final String number;
        private final String owner;
        private double balance;
        private boolean active = true;
        private static int accNumber = 1;

        Account(final String owner) {
            this.owner = owner;
            this.number = "" + accNumber++;
        }

        @Override
        public double getBalance() {
            return this.balance;
        }

        @Override
        public String getOwner() {
            return this.owner;
        }

        @Override
        public String getNumber() {
            return this.number;
        }

        @Override
        public boolean isActive() {
            return this.active;
        }

        @Override
        public void deposit(final double amount) throws InactiveException, IllegalArgumentException {

            if (!this.active) {
                throw new InactiveException();
            }

            if (amount < 0) {
                throw new IllegalArgumentException();
            }

            this.balance = this.balance + amount;
        }

        @Override
        public void withdraw(final double amount)
                throws InactiveException, OverdrawException, IllegalArgumentException {

            if (!this.active) {
                throw new InactiveException();
            }

            if (this.balance < amount) {
                throw new OverdrawException();
            }

            if (amount < 0) {
                throw new IllegalArgumentException();
            }

            this.balance = this.balance - amount;
        }

    }

}
